# Aula 10 - Disciplina Introdução a BigData. 
## Objetivo
Manipular o arquivo CSV proposto na aula anterior. Gerando um novo arquivo com algumas características.

## Desafio! Preparar dados

Nessa aula iremos executar um código em Python e realizar pequenas modificações.

Média Mensal dos valores da cotação do dolar
Média Mensal dos Valores da soja

Minimo  Mensal dos valores da cotação do dolar
Minimo  Mensal Mensal dos Valores da soja

Máximo  Mensal dos valores da cotação do dolar
Máximo  Mensal Mensal dos Valores da soja

## Nosso problema:

* Um produtor de soja e precisa saber o melhor momento para vender o meu produto?

* As varáveis mais significativos para exportação são o preço do Dolar e Cotação da commodity no exterior.



## Perguntas:

- Como organizar dados de catações de Dolar e do produto Soja?

- O que é Máximo, Mínimo, Média e Mediana?

- Você pode imaginar outras aplicações para o conteúdo aprendido?

## Ambiente de Desenvolvimento

Ser possível executar um código em Python.
Exemplo: `python lerCSV.py` no Terminal.

a) Usar a IDE Geany [LINK] (https://www.geany.org/)

b) Usar o Python [LINK] (https://www.python.org/)

## Recursos

Computadores.

### Como preparar o ambiente com Python

Instalando o Python em diferentes ambientes [LINK] (http://www.programeempython.com.br/blog/instalando-o-python/)


### Como clonar o Projeto

`git clone git@gitlab.com:nallaworks/Ibigdata_Aula10.git`

## Entrega
Pessoal a entrega será: 

Entrega!
[bit.ly/bdag1] (http://bit.ly/bdag1)
* Usando os arquivos fornecidos em aula com dados de soja e dolar, gerar um código que salve um alquivo com o nome dados_novos.csv contendo os dados.

Média Mensal dos valores da cotação do dolar
Média Mensal dos Valores da soja

Minimo  Mensal dos valores da cotação do dolar
Minimo  Mensal Mensal dos Valores da soja

Máximo  Mensal dos valores da cotação do dolar
Máximo  Mensal Mensal dos Valores da soja


### Enviar para o email:

* allan@fatecpompeia.edu.br
* Assunto ”[IntBigData] – Dados de soja e dolar”.
* Até dia 04/05.
* Outros dados:

* Prof: 
* Whats: (14) 98199-7228


Grato, 
Allan